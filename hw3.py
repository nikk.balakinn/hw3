# 1. Написать любую детерменированную функцию (Детерменированная функция = функция, которая возвращает одно и тоже вне
# зависимости от парамеметров)

def return_None():
    return None

#2. Написать функцию, которая вернет True если число четное и False если не четное


def r(i):
    return i % 2 ==0
i = int(input("i= "))
print(r(i))

#3. Напишите функция is_prime, которая принимает 1 аргумент (число) и возвращает True, если число простое, иначе False
#   Простое число - это число, которое делится без остатка только на себя и на 1

def if_prime(i):
    for b in range(2, i):
        if i % b == 0:
            return False

    return True
i= int(input("Введите число"))
print(if_prime(i))

# 4. Напишите функцию, которая принимает 1 аргумент (строка) и выполняет следующие действия на каждую из букв строки:
# i - инкремент (+1)
# d - дикремент (-1)
# s - возведение в квадрат
# o - добавить число в результативный список
# остальные буквы игнорируются
# Исходное число = 0
# Результативный список = []
# Вернуть результативный список
# parse("iiisdoso")  ==>  [8, 64] <- это как пример


def parse(operations):
    start_num = 0
    res = []
    for op in operations:
        if op == 'i':
            start_num += 1
        elif op == 'd':
            start_num -= 1
        elif op == 's':
            start_num **= 2
        elif op == 'o':
            res.append(start_num)
    return res

print(parse("iiisdoso"))


# 5. Написать функцию, которая из строки делает datetime и возвращает результат, если введенный
# формат даты неверный - возвращать None. Аргументы - срока-дата, формат,
# по которому можно привести строку в datetime

from datetime import datetime


def dt(date_str, format_str):
    try:
        return datetime.strptime(date_str, format_str)
    except:
        return None
print(dt('14.01.2022', '%d.%m.%Y'))
